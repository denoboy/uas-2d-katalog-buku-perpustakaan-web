-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Bulan Mei 2020 pada 15.17
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(5) NOT NULL,
  `judul_buku` varchar(35) DEFAULT NULL,
  `pengarang` varchar(35) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `penerbit` varchar(35) DEFAULT NULL,
  `sinopsis` text,
  `foto` varchar(100) DEFAULT NULL,
  `id_kategori` varchar(5) NOT NULL,
  `id_rak` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `judul_buku`, `pengarang`, `tahun`, `penerbit`, `sinopsis`, `foto`, `id_kategori`, `id_rak`) VALUES
(2, 'Catatan Juang', 'Fiersa Besari', '2012', 'Grasindo', 'Tak sengaja Suar menemukan sebuah buku bersampul merah ketika hendak turun dari angkutan umum. Buku catatan yang tampak lusuh. Suar menanyakan sesiapa pemiliknya, tetapi tidak ada yang tahu. Sakit kepala yang ia rasakan sedari tadi, membuatnya bergegas untuk memasukkan buku tersebut kedalam tas jinjing dan melangkah menuju rumah indekosnnya untuk istirahat', 'novel2.jpg', '1', '1'),
(3, 'Konspirasi Alam Semesta', 'Fiersa Besari', '2013', 'Grasindo', 'Juang merupakan sosok yang begitu menarik. Memiliki ayah seorang eks tapol, ia sempat memiliki berbagai pengalaman tak menyenangkan. Bahkan ia sendiri berjarak dengan ayahnya. Hubungannya dengan sang ayah begitu dingin. Hanya ibunya yang membuat Juang bisa kembali ke keluarga, sayangnya kebersamaannya dengan sang ibu tak bisa berlangsung selamanya.', 'novel3.jpg', '1', '2'),
(4, 'Hukum Sosial Tata Negara Indonesia', 'Sri Soemantri', '2015', 'Rosda', 'Hukum Sosial di Indonesia', 'sosial1.jpg', '3', '2'),
(5, 'Ekonomi Maritim Indonesia', 'Ade Prasetia', '2016', 'GPU', 'Ekonomi maritim di Indonesia', 'sosial2.jpg', '3', '2'),
(8, 'Teknologi', 'Izzul', '2010', 'Gramedia', 'Network Tweaking dan Hacking', 'DC20200518201112.jpg', '4', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'novel'),
(2, 'umum'),
(3, 'sosial'),
(4, 'teknologi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rak`
--

CREATE TABLE `rak` (
  `id_rak` int(11) NOT NULL,
  `nama_rak` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rak`
--

INSERT INTO `rak` (`id_rak`, `nama_rak`) VALUES
(1, 'A1'),
(2, 'B2'),
(3, 'C3');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id_rak`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `rak`
--
ALTER TABLE `rak`
  MODIFY `id_rak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
