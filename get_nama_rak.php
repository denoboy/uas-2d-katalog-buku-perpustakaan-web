<?php
    $DB_NAME = "buku";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "select id_rak, nama_rak from rak order by nama_rak asc";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_rak = array();
            while($nm_rak = mysqli_fetch_assoc($result)){
                array_push($nama_rak, $nm_rak);
            }
            echo json_encode($nama_rak);
        }
    }
?>