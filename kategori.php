<?php

$DB_NAME = "buku";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $nama_kategori = $_POST['nama_kategori'];
    $sql = "SELECT bk.id_buku, bk.judul_buku, bk.pengarang, bk.tahun, bk.penerbit, bk.sinopsis, concat('http://192.168.0.114/uas/cover/',foto) as url, kt.nama_kategori, rk.nama_rak
    FROM buku bk, kategori kt, rak rk
    WHERE bk.id_kategori = kt.id_kategori AND bk.id_rak = rk.id_rak AND kt.nama_kategori = '$nama_kategori'";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result)>0){
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_ms = array();
        while($ms = mysqli_fetch_assoc($result)){
            array_push($data_ms, $ms);
        }
        echo json_encode($data_ms);
    }
}
?>