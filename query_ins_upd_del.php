<?php
    $DB_NAME = "buku";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array();
        $respon['kode'] = '000';

        switch($mode){
            case "insert":
                $judul_buku = $_POST['judul_buku'];
                $pengarang = $_POST['pengarang'];
                $tahun = $_POST['tahun'];
                $penerbit = $_POST['penerbit'];
                $sinopsis = $_POST['sinopsis'];
                $imstr = $_POST['image'];
                $file = $_POST['file'];
                $path = "cover/";
                $nama_kategori = $_POST['nama_kategori'];
                $nama_rak = $_POST['nama_rak'];

                $sql = "select id_kategori from kategori where nama_kategori='$nama_kategori'";
                $sql2 = "select id_rak from rak where nama_rak='$nama_rak'";
                $result = mysqli_query($conn,$sql);
                $result2 = mysqli_query($conn,$sql2);
                if((mysqli_num_rows($result)>0) && (mysqli_num_rows($result2)>0)){
                    $data = mysqli_fetch_assoc($result);
                    $data2 = mysqli_fetch_assoc($result2);
                    $id_kategori = $data['id_kategori'];
                    $id_rak = $data2['id_rak'];

                    $sql = "insert into buku(judul_buku, pengarang, tahun, penerbit, sinopsis, foto, id_kategori,id_rak) values('$judul_buku', '$pengarang', '$tahun', '$penerbit','$sinopsis', '$file', '$id_kategori','$id_rak')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file, base64_decode($imstr)) == false){
                            $sql = "delete from buku where judul_buku='$judul_buku'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon);
                            exit();
                        }
                        else{
                            echo json_encode($respon);
                            exit();
                        }
                    }
                    else{
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    }
                }
            break;

            case "update":
                $id_buku = $_POST['id_buku'];
                $judul_buku = $_POST['judul_buku'];
                $pengarang = $_POST['pengarang'];
                $tahun = $_POST['tahun'];
                $penerbit = $_POST['penerbit'];
                $sinopsis = $_POST['sinopsis'];
                $imstr = $_POST['image'];
                $file = $_POST['file'];
                $path = "cover/";
                $nama_kategori = $_POST['nama_kategori'];
                $nama_rak = $_POST['nama_rak'];

                $sql = "select id_kategori from kategori where nama_kategori='$nama_kategori'";
                $sql2 = "select id_rak from rak where nama_rak='$nama_rak'";
                $result = mysqli_query($conn,$sql);
                $result2 = mysqli_query($conn,$sql2);
                if((mysqli_num_rows($result)>0) && (mysqli_num_rows($result2)>0)){
                    $data = mysqli_fetch_assoc($result);
                    $data2 = mysqli_fetch_assoc($result2);
                    $id_kategori = $data['id_kategori'];
                    $id_rak = $data2['id_rak'];

                    $sql = "";
                    if($imstr == ""){
                        $sql = "update buku set judul_buku='$judul_buku', pengarang='$pengarang', penerbit = '$penerbit', sinopsis='$sinopsis', id_kategori='$id_kategori', id_rak='$id_rak' where id_buku='$id_buku'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon);
                            exit();
                        }
                        else{
                            $respon['kode'] = "111";
                            echo json_encode($respon);
                            exit();
                        }
                    }
                    else{
                            if(file_put_contents($path.$file, base64_decode($imstr)) == false){
                                $respon['kode'] = "111";
                                echo json_encode($respon);
                                exit();
                            }
                            else{
                                $sql = "update buku set judul_buku='$judul_buku', pengarang='$pengarang', penerbit = '$penerbit', sinopsis='$sinopsis', id_kategori='$id_kategori', id_rak='$id_rak', foto='$file' where id_buku='$id_buku'";
                                $result = mysqli_query($conn,$sql);
                                if($result){
                                    echo json_encode($respon);
                                    exit();
                                }
                                else{
                                    $respon['kode'] = "111";
                                    echo json_encode($respon);
                                    exit();
                                }
                            }
                        }
                }
            break;

            case "delete":
                $id_buku = $_POST['id_buku'];
                $sql = "select foto from buku where id_buku='$id_buku'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['foto'];
                        $path = "cover/";
                        unlink($path.$photos);
                    }

                    $sql = "delete from buku where id_buku='$id_buku'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon);
                        exit();
                    }
                    else{
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    }
                }
            break;
        }
    }
?>